﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="用户注册.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 21px;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            height: 22px;
        }
        .auto-style4 {
            width: 76px;
        }
        .auto-style5 {
            height: 21px;
            width: 76px;
        }
        .auto-style6 {
            height: 22px;
            width: 76px;
        }
        .auto-style7 {
            height: 23px;
            width: 76px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width:100%;">
                <tr>
                    <td colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>用户注册</td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;&nbsp; 用户名:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </td>

                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;&nbsp;&nbsp; 密码:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    </td>

                </tr>
                <tr>
                    <td class="auto-style4">密码确认:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">密保问题:</td>
                    <td colspan="3">
                        <asp:DropDownList ID="DropDownList2" runat="server" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                            <asp:ListItem>--自定义--</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4"></td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">密保答案:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                    </td>
                </tr>
                                <tr>
                    <td class="auto-style4">&nbsp;&nbsp;&nbsp; 邮箱:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                                    </td>
                </tr>
                                <tr>
                    <td class="auto-style7">联系电话:</td>
                    <td colspan="3" class="auto-style2">
                        <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                    </td>
                </tr>
                                <tr>
                    <td class="auto-style4">&nbsp;&nbsp;&nbsp; 性别:</td>
                    <td colspan="3">
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>男</asp:ListItem>
                            <asp:ListItem>女</asp:ListItem>
                        </asp:RadioButtonList>
                                    </td>
                </tr>
                                <tr>
                    <td class="auto-style5">&nbsp;&nbsp;&nbsp; 专业:</td>
                    <td class="auto-style1" colspan="3">
                        <asp:DropDownList ID="DropDownList1" runat="server">
                            <asp:ListItem>请选择</asp:ListItem>
                            <asp:ListItem>土木工程</asp:ListItem>
                            <asp:ListItem>财经商贸</asp:ListItem>
                            <asp:ListItem>教育艺术</asp:ListItem>
                            <asp:ListItem>软件技术</asp:ListItem>
                        </asp:DropDownList>
                                    </td>
                </tr>
                                <tr>
                    <td class="auto-style6">&nbsp;&nbsp;&nbsp; 爱好:</td>
                    <td class="auto-style3" colspan="3">
                        <asp:CheckBoxList ID="CheckBoxList1" runat="server" RepeatDirection="Horizontal" ViewStateMode="Disabled">
                            <asp:ListItem>抽烟</asp:ListItem>
                            <asp:ListItem>喝酒</asp:ListItem>
                            <asp:ListItem>烫头发</asp:ListItem>
                            <asp:ListItem>打游戏</asp:ListItem>
                            <asp:ListItem>吃饭</asp:ListItem>
                            <asp:ListItem>睡觉</asp:ListItem>
                        </asp:CheckBoxList>
                                    </td>
                </tr>
                                <tr>
                    <td class="auto-style7">&nbsp;&nbsp;&nbsp; 头像:</td>
                    <td class="auto-style2" colspan="3">
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                    </td>
                </tr>
                                <tr>
                    <td class="auto-style4">自我介绍:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox9" runat="server" Height="112px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                                    </td>
                </tr>
                                <tr>
                    <td class="auto-style4"></td>
                    <td colspan="3">
                        <asp:Button ID="Button1" runat="server" Text="注册" />
&nbsp;&nbsp;
                        <asp:Button ID="Button2" runat="server" Text="取消" OnClick="Button2_Click" />
                                    </td>
                </tr>
            </table>
        </div>
    </form>
    <p>
        &nbsp;</p>
</body>
</html>
