﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace 第三章练习1
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = Session["user"].ToString();
            Label2.Text = Session["gender"].ToString();
            Label3.Text = Session["hobby"].ToString();
            Label4.Text = Session["birthday"].ToString();
            Label5.Text = Session["email"].ToString();
            Label6.Text = Session["city"].ToString();
        }

    }
}