﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="第三章练习1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 23px;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            width: 100%;
        }
        .auto-style4 {
            height: 23px;
            width: 601px;
        }
        .auto-style5 {
            width: 601px;
        }
        .auto-style6 {
            height: 23px;
            width: 601px;
        }
        .auto-style7 {
            margin-left: 0px;
        }
        .auto-style8 {
            height: 23px;
            width:80px;
        }
        .auto-style9 {
            width: 80px;
        }
        .auto-style10 {
            height: 23px;
            width:80px;
        }
        .auto-style11 {
            width: 80px;
            height: 23px;
        }
        .auto-style12 {
            width: 601px;
            height: 23px;
        }
        .auto-style13 {
            height: 23px;
        }
        .auto-style14 {
            width: 80px;
            height: 23px;
        }
        .auto-style15 {
            width: 601px;
            height: 23px;
        }
        .auto-style16 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        <table class="auto-style3">
            <tr>
                <td class="auto-style8">用户名:</td>
                <td class="auto-style4">
                    <asp:TextBox ID="TextBox1" runat="server" CssClass="auto-style7"></asp:TextBox>
                </td>
                <td class="auto-style1"></td>
            </tr>
            <tr>
                <td class="auto-style9">密码:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style11">确认密码:</td>
                <td class="auto-style12">
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style13"></td>
            </tr>
            <tr>
                <td class="auto-style10">性别:</td>
                <td class="auto-style6">
                    <asp:RadioButton ID="RadioButton1" runat="server" Text="男" GroupName="gender" />
                    <asp:RadioButton ID="RadioButton2" runat="server" Text="女" GroupName="gender" />
                </td>
                <td class="auto-style2"></td>
            </tr>
            <tr>
                <td class="auto-style9">兴趣爱好:</td>
                <td class="auto-style5">
                    <asp:CheckBox ID="CheckBox1" runat="server" Text="编程" />
                    <asp:CheckBox ID="CheckBox2" runat="server" Text="唱歌" />
                    <asp:CheckBox ID="CheckBox3" runat="server" Text="打球" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style14">出生日期:</td>
                <td class="auto-style15">
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style16"></td>
            </tr>
            <tr>
                <td class="auto-style9">邮箱:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style9">详细地址:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBox6" runat="server" TextMode="MultiLine" Width="200px"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style9">照片:</td>
                <td class="auto-style5">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="Button1" runat="server" Text="上传" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="Button2" runat="server" Text="注册" OnClick="Button2_Click" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">管理员提示</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                    <asp:TextBox ID="TextBox7" runat="server" Height="80px" TextMode="MultiLine" Width="270px">用户需知：我们将保护您的隐私并保证您提供的个人资料的保密性。我们收集的个人资料仅用于为您提供服务。除此之外，我们只会在您允许的情况下才使用您提供的资料；否则本网站绝不会与第三方共享您的资料</asp:TextBox>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
