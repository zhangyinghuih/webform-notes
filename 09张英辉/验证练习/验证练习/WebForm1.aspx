﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="验证练习.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     <style type="text/css">
        .auto-style3 {
            width: 100%;
        }
        .auto-style5 {
            width: 601px;
        }
        .auto-style9 {
            width: 80px;
        }
        .auto-style10 {
            height: 23px;
            width:80px;
        }
        .auto-style11 {
            width: 80px;
            height: 23px;
        }
        .auto-style12 {
            width: 601px;
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
         <table class="auto-style3">
            <tr>
                <td class="auto-style8">用户名:</td>
                <td class="auto-style4">
                    <asp:TextBox ID="TextBox1" runat="server" CssClass="auto-style7"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="TextBox1" runat="server" ErrorMessage="用户名不能为空"></asp:RequiredFieldValidator>
                </td>
                <td class="auto-style1"></td>
            </tr>
            <tr>
                <td class="auto-style9">密码:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="TextBox2" runat="server" ErrorMessage="密码不能为空"></asp:RequiredFieldValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style11">确认密码:</td>
                <td class="auto-style12">
                    <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="TextBox3" runat="server" ErrorMessage="密码不能为空"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" Operator="Equal" ControlToValidate="TextBox3" ControlToCompare="TextBox2" runat="server" ErrorMessage="密码不一致"></asp:CompareValidator>
                </td>
                <td class="auto-style13"></td>
            </tr>
            <tr>
                <td class="auto-style14">入学日期:</td>
                <td class="auto-style15">
                    <asp:TextBox ID="TextBox5" runat="server" TextMode="Date"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" Type="Date" ControlToValidate="TextBox5" runat="server" ErrorMessage="2019-09-01至今"></asp:RangeValidator>
                </td>
                <td class="auto-style16"></td>
            </tr>
            <tr>
                <td class="auto-style9">年龄:</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="TextBox4" MinimumValue="0" MaximumValue="130" runat="server" ErrorMessage="年龄0-130"></asp:RangeValidator>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="注册" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
