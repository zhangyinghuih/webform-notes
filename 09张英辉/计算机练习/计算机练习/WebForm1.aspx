﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="计算机练习.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
            <div align="center" style="height:auto 0">
             <h1>计算器</h1><br />
             请输入第一个数<br />
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><br />
            请输入第二个数<br />
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><br />
            请选择要执行的运算法则<br />
            <asp:Button ID="Button1" runat="server" Text="加" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="减" OnClick="Button2_Click" />
            <asp:Button ID="Button3" runat="server" Text="乘" OnClick="Button3_Click" />
            <asp:Button ID="Button4" runat="server" Text="除" OnClick="Button4_Click" />
            <br />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
