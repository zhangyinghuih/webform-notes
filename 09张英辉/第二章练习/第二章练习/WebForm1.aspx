﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="第二章练习.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 44px;
        }
        .auto-style2 {
            width: 245px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
         <table style="width:100%;">
            <tr>
                <td colspan="2" align="center">顾客信息登记</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">姓名</td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">年龄</td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">爱好</td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox5" runat="server" Height="250px" TextMode="MultiLine" Width="250px"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">&nbsp;</td>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="提交" />
                </td>
            </tr>
        </table>
        <asp:Label ID="Label1" runat="server" Text="您好，现在的时间是："></asp:Label>
        <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        <br />
         <asp:TextBox ID="TextBox4" runat="server" Height="100px" TextMode="MultiLine" Width="150px"></asp:TextBox>
    </form>
</body>
</html>
